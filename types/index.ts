export interface Item {
  id: number;
  sellerId: number;
  title: string;
  makeId: number;
  makeKey: string;
  modelKey: string;
  images: (Images)[] | null;
  price: Price;
  vat: string;
  contact: Contact;
  isNew: boolean;
  isConditionNew: boolean;
  isPreRegistration: boolean;
  category: string;
  description: string;
  attributes: (Attributes)[] | null;
  features: (string)[] | null;
  hasDamage: boolean;
  isDamageCase: boolean;
  htmlDescription: string;
  segment: string;
  vc: string;
  created: number;
  adMobTargeting: AdMobTargeting;
}

export interface Images {
  uri: string;
  set: string;
}

export interface Price {
  gross: string;
  grs: GrsOrNt;
  net: string;
  nt: GrsOrNt;
  type: string;
  vat: number;
}

export interface GrsOrNt {
  amount: number;
  currency: string;
}

export interface Contact {
  type: string;
  country: string;
  enumType: string;
  name: string;
  address1: string;
  address2: string;
  phones: (PhonesEntity)[] | null;
  latLong: LatLong;
  withMobileSince: string;
  imprint: string;
  canSendCcMail: boolean;
}

export interface PhonesEntity {
  type: string;
  number: string;
  uri: string;
}

export interface LatLong {
  lon: number;
  lat: number;
}

export interface Attributes {
  label: string;
  tag: string | null;
  value: string;
}

export interface AdMobTargeting {
  make: string;
  model: string;
  price: string;
  dealer: string;
  channel: string;
  preis: string;
  ma: string;
  kw: string;
  ez: string;
  intid: string;
  advid: string;
}
