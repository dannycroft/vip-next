import { render } from '@testing-library/react';
import Layout from './layout';

const MockChild = () => (<h1>Test</h1>)

test('renders the header', async () => {
  const { container } = render(<Layout children={<MockChild />} />);

  expect(container).toMatchSnapshot();
});
