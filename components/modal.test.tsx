import { render, fireEvent } from '@testing-library/react';
import { screen } from '@testing-library/dom';
import { axe, toHaveNoViolations } from 'jest-axe';

import Modal from './modal';

expect.extend(toHaveNoViolations);

test('renders modal with image', () => {
  const { container } = render(
    <Modal imgSrc="../images/batmobile.png" onClose={() => null} isOpen={true} />,
  );

  const image = container.querySelector('img');
  expect(image?.src).toEqual('http://localhost/images/batmobile.png');
});

test('renders the modal without image', () => {
  const { container } = render(<Modal onClose={() => null} isOpen={true} />);

  const image = container.querySelector('img');
  expect(image).toBeNull();
});

test('renders the modal with title', () => {
  render(<Modal title="The Penguin" onClose={() => null} isOpen={true} />);

  const header = screen.getByText('The Penguin');
  expect(header).toHaveAttribute('id', 'modal__Label');
});

test('renders the modal without title', () => {
  const { container } = render(<Modal onClose={() => null} isOpen={true} />);

  const header = container.querySelector('header');
  expect(header).toBeNull();
});

test('modal is accessible', async () => {
  const { container } = render(
    <Modal imgSrc="../batmobile.jpg" title="Batmobile" onClose={() => null} isOpen={true} />,
  );

  const results = await axe(container);
  expect(results.violations).toHaveLength(0);
});

test('modal calls onClose successfully', () => {
  const onClose = jest.fn();
  render(<Modal imgSrc="../batmobile.jpg" title="Batmobile" onClose={onClose} isOpen={true} />);

  const button = screen.getByLabelText('Close Modal');

  onClose.mockClear();
  fireEvent.click(button);
  expect(onClose).toHaveBeenCalledTimes(1);
});
