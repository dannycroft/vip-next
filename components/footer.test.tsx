import { render } from '@testing-library/react';
import Footer from './footer';

test('renders the footer', async () => {
  const { container } = render(<Footer />);

  expect(container).toMatchSnapshot();
});
