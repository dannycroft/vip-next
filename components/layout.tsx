import Head from "next/head";
import styled from "styled-components";

import { theme } from '../lib/theme';
import Header from "./header";
import Footer from "./footer";

interface Props {
  children: React.ReactNode;
  title?: string;
}

const Main = styled.main`
  max-width: 940px;
  margin: 0 auto;
  display: grid;
  grid-template-columns: 3fr;
  grid-gap: 10px;
  padding: ${(props) => props.theme.spaces[2]} ${(props) => props.theme.spaces[2]};
`;

Main.defaultProps = { theme };

const Layout = ({
  children,
  title = "mobile.de – Gebrauchtwagen und Neuwagen – Deutschlands größter Fahrzeugmarkt",
}: Props) => (
  <>
    <Head>
      <title>{title}</title>
    </Head>

    <Header />

    <Main>{children}</Main>

    <Footer />
  </>
);

export default Layout;
