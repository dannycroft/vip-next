import { render, fireEvent } from '@testing-library/react';
import { screen } from '@testing-library/dom';
import { axe, toHaveNoViolations } from 'jest-axe';

import type { Item } from '../types';

import Gallery from './gallery';
import mockItem from '../lib/hiring-challenge.json';

expect.extend(toHaveNoViolations);

test('gallery is accessible', async () => {
  const { container } = render(<Gallery item={mockItem as Item} />);

  const results = await axe(container);
  expect(results.violations).toHaveLength(0);
});

test('renders the gallery', async () => {
  const { container } = render(<Gallery item={mockItem as Item} />);

  expect(container).toMatchSnapshot();
});

// Sorry for the TODO list
test.todo('image card correctly formats image URLs');
test.todo('image card triggers click handler');
test.todo('image element contains largeSrc data attribute');
test.todo('gallery sets the selected item correctly');
test.todo('gallery opens the modal successfully');
test.todo('gallery doesn\'t render modale with no selected item');
