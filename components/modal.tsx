import styled from 'styled-components';

interface Wrapper {
  isOpen?: boolean;
}

const Wrapper = styled.div<Wrapper>`
  position: fixed;
  inset: 0;
  background-color: rgba(0, 0, 0, 0.95);
  display: ${(props) => (props.isOpen ? 'grid' : 'none')};
  justify-content: center;
  align-items: center;
`;

const Title = styled.header`
  padding: 8px;
  color: #fff;
`;

const Content = styled.div`
  position: relative;
`;

const Image = styled.img`
  width: 100%;
  height: auto;
  margin: 0 auto;
`;

const CloseButton = styled.button`
  background: transparent;
  color: #fff;
  cursor: pointer;
  border: 0;
  width: 46px;
  height: 46px;
  font-size: 30px;
  position: absolute;
  right: 0;
  top: 0;
`;

interface Modal {
  onClose: () => void;
  isOpen: boolean;
  title?: string;
  imgSrc?: string;
}

const Modal = ({ isOpen = false, title, imgSrc, onClose }: Modal) => {
  return (
    <Wrapper
      isOpen={isOpen}
      role={'dialog'}
      aria-modal={true}
      aria-labelledby="modal__Label"
      aria-label={`Modal showing ${title}`}>
      <CloseButton onClick={onClose} aria-label="Close Modal">✕</CloseButton>
      <Content>
        {title && <Title id="modal__Label">{title}</Title>}
        {imgSrc && <Image alt={'Test'} src={imgSrc} />}
      </Content>
    </Wrapper>
  );
};

export default Modal;
