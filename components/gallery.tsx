import { MouseEvent, useState } from 'react';
import dynamic from 'next/dynamic';
import styled from 'styled-components';

import type { Item, Images } from '../types';

const LazyModal = dynamic(() => import('./modal'));

export enum ImageSize {
  Small = '$_2.jpg',
  Large = '$_27.jpg',
}

interface Gallery {
  item: Item;
}

interface ImageCard {
  handler: (event: MouseEvent<HTMLButtonElement>) => void;
  altText: string;
  image: Images;
}

const Button = styled.button`
  padding: 0;
  margin: 6px;
  border: 1px solid #d8d8d8;
  background: transparent;
  cursor: pointer;
`;

export const ImageCard = ({ handler, altText, image }: ImageCard) => {
  const smallImageUrl = `https://` + image.uri.replace('$', ImageSize.Small);
  const largeImageUrl = `https://` + image.uri.replace('$', ImageSize.Large);

  return (
    <Button onClick={handler}>
      <img
        alt={altText}
        src={smallImageUrl}
        width={200}
        height={133}
        data-large-src={largeImageUrl}
      />
    </Button>
  );
};

const GalleryWrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(250px, 1fr));
  grid-auto-rows: minmax(150px, auto);
`;

interface SelectedItem {
  src?: string;
  title?: string;
}

const Gallery = ({ item }: Gallery) => {
  const { images } = item;
  const itemName = `${item.makeKey} ${item.modelKey}`;
  const [selectedItem, setSelectedItem] = useState<SelectedItem>();
  const [isModelOpen, setIsModalOpen] = useState<boolean>(false);

  const imageClickHandler = (event: MouseEvent<HTMLButtonElement>) => {
    const img = event.currentTarget.querySelector('img');

    if (img && img.dataset.largeSrc) {
      setIsModalOpen(true);
      setSelectedItem({
        src: img.dataset.largeSrc,
        title: img.alt,
      });
    }
  };

  const cards = images?.map((image, index) => (
    <ImageCard
      handler={imageClickHandler}
      altText={`Photo ${index + 1} of ${images.length} - ${itemName}`}
      image={image}
      key={index}
    />
  ));

  return (
    <>
      <GalleryWrapper>{cards}</GalleryWrapper>
      {!!selectedItem && (
        <LazyModal
          onClose={() => setIsModalOpen(false)}
          isOpen={isModelOpen}
          imgSrc={selectedItem?.src}
          title={selectedItem?.title}
        />
      )}
    </>
  );
};

export default Gallery;
