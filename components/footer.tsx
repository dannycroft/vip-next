import styled from 'styled-components';

import { theme } from '../lib/theme';

const Footer = styled.footer`
  max-width: ${(props) => props.theme.sizes.layoutWidth};
  margin: 0 auto;
  grid-column: 1 / -1;
  padding: ${(props) => props.theme.spaces[2]} ${(props) => props.theme.spaces[2]};
`;

Footer.defaultProps = { theme };

const FooterGroup = () => (
  <Footer>
    <p>&copy; mobile.de</p>
  </Footer>
);

export default FooterGroup;
