import { render } from '@testing-library/react';
import Header from './header';

test('renders the header', async () => {
  const { container } = render(<Header />);

  expect(container).toMatchSnapshot();
});
