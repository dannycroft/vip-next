import styled from 'styled-components';

import { theme } from '../lib/theme';

const Header = styled.header`
  max-width: ${(props) => props.theme.sizes.layoutWidth};
  margin: 0 auto;
  grid-column: 1 / -1;
  padding: ${(props) => props.theme.spaces[2]} ${(props) => props.theme.spaces[2]};
`;

Header.defaultProps = { theme };

const HeaderGroup = () => (
  <Header>
    <img src="/mobile-de.svg" alt="mobile.de" width={114} height={28} />
  </Header>
);

export default HeaderGroup;
