export const theme = {
  spaces: [".25rem", ".5rem", "0.75rem", "1rem", "1.5rem"],
  sizes: {
    layoutWidth: "940px",
  },
  colors: {
    primary: "#0070f3",
  },
};
