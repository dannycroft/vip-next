import { GetServerSideProps, NextPage } from 'next';
import ErrorPage from 'next/error';

import type { Item, Images } from '../../types';
import Layout from '../../components/layout';
import Gallery from '../../components/gallery';

interface Props {
  item: Item;
}

const ViewItemPage: NextPage<Props> = ({ item }) => {
  if (!item) {
    return <ErrorPage statusCode={404} />;
  }

  return (
    <Layout>
      <h1>
        {item.makeKey} {item.modelKey}
      </h1>
      <Gallery item={item} />
    </Layout>
  );
};

export const getServerSideProps: GetServerSideProps = async ({ params, res }) => {
  try {
    if (params) {
      const { id } = params;
      const result = await fetch(`http://localhost:3000/api/vip/${id}`);
      const item: Item = await result.json();

      return {
        props: { item },
      };
    } else {
      throw new Error('No params set');
    }
  } catch {
    res.statusCode = 500;
    return {
      props: {},
    };
  }
};

export default ViewItemPage;
