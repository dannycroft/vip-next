import { NextApiHandler } from "next";

import data from "../../../lib/hiring-challenge.json";

const item: NextApiHandler = async (req, res) => {
  const { id } = req.query;
  const item = [data].find(x => String(x.id) === String(id));

  if (item) {
    res.status(200).json(item);
  } else {
    res.status(404).end();
  }
}

export default item;
