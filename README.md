# VIP Example

## Getting Started

 - `npm install`
 - `npm run build`
 - `npm run start`
 - Visit http://localhost:3000/vip/97045

### Running Tests

 - `npm run test`

### Starting Development Mode

- `npm run dev`


## Notes

 - I used Next.js because I wanted to server-side render critial SEO related parts of the page and Next gives you alot with little effort. I've used Next.js recommended naming, and file conventions. I don't use Next.js alot so expect dragons.
 - I tried to test as much as I could in my self allocated time allowance. I tested a large part but left `todos` for the rest.
